package com.floor8.constraint;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class AllDIffConstraintTest {
    Collection<Variable> inputList;

    @Before
    public void before() {
        this.inputList = new ArrayList<>();
    }

    @Test
    public void visibleDisjointSubsets() {
        // AllDiffConstraint.visibleDisjointSubset();
    }

    @Test
    public void hiddenDisjointSubsets() {
        // AllDiffConstraint.hiddenDisjointSubset()
    }
}

class TestVariable extends AbstractVariable {
    public TestVariable() {
        this.setDomain(new GenericDiscreteDomain(Arrays.asList(1, 2, 3)));
    }
}
