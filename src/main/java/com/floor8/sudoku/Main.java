package com.floor8.sudoku;

import com.floor8.constraint.ConstraintSolver;
import com.floor8.constraint.ConstraintViolationException;
import com.floor8.constraint.Variable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class Main {
    // 0 represents a unknown (null) value
    private static Integer[] emptyPuzzle = {
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0
    };

    private static Integer[] visibleDisjointTest = {
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            3,0,0,0,0,3,0,0,0,
            4,0,0,0,0,4,0,0,0,
            5,0,0,0,0,5,0,0,0,
            6,0,0,0,0,6,0,0,0,
            7,0,0,0,0,7,0,0,0,
            8,0,0,0,0,8,0,0,0,
            9,0,0,0,0,9,0,0,0
    };

    private static Integer[] multipleVisibleDisjointTest = {
            0,0,0,4,5,6,7,8,9,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,
            1,2,3,0,0,0,0,0,0
    };

    private static Integer[] hiddenDisjointTest = {
            0,0,0,0,6,0,0,0,0,
            0,0,0,0,4,2,7,3,6,
            0,0,6,7,3,0,0,4,0,
            0,9,4,0,0,0,0,6,8,
            0,0,0,0,9,6,4,0,7,
            6,0,7,0,5,0,9,2,3,
            1,0,0,0,0,0,0,8,5,
            0,6,0,0,8,0,2,7,1,
            0,0,5,0,1,0,0,9,4
    };

    private static Integer[] easyPuzzle = {
            0,6,0,3,0,0,8,0,4,
            5,3,7,0,9,0,0,0,0,
            0,4,0,0,0,6,3,0,7,
            0,9,0,0,5,1,2,3,8,
            0,0,0,0,0,0,0,0,0,
            7,1,3,6,2,0,0,4,0,
            3,0,6,4,0,0,0,1,0,
            0,0,0,0,6,0,5,2,3,
            1,0,2,0,0,9,0,8,0
    };
    // 0 6 0 3 0 0 8 0 4 5 3 7 0 9 0 0 0 0 0 4 0 0 0 6 3 0 7 0 9 0 0 5 1 2 3 8 0 0 0 0 0 0 0 0 0 7 1 3 6 2 0 0 4 0 3 0 6 4 0 0 0 1 0 0 0 0 0 6 0 5 2 3 1 0 2 0 0 9 0 8 0
    /*Solution
            2,6,1,3,7,5,8,9,4
            5,3,7,8,9,4,1,6,2
            9,4,8,2,1,6,3,5,7
            6,9,4,7,5,1,2,3,8
            8,2,5,9,4,3,6,7,1
            7,1,3,6,2,8,9,4,5
            3,5,6,4,8,2,7,1,9
            4,8,9,1,6,7,5,2,3
            1,7,2,5,3,9,4,8,6
     */

    private static Integer[] mediumPuzzle1 = {
            7,9,0,0,0,0,3,0,0,
            0,0,0,0,0,6,9,0,0,
            8,0,0,0,3,0,0,7,6,
            0,0,0,0,0,5,0,0,2,
            0,0,5,4,1,8,7,0,0,
            4,0,0,7,0,0,0,0,0,
            6,1,0,0,9,0,0,0,8,
            0,0,2,3,0,0,0,0,0,
            0,0,9,0,0,0,0,5,4
    };

    private static Integer[] mediumPuzzle2 = {
            2,0,4,1,0,0,0,0,0,
            0,0,0,5,0,3,6,0,7,
            0,0,0,9,0,0,4,0,0,
            9,0,0,4,0,0,0,1,0,
            6,5,0,0,1,0,0,7,4,
            0,2,0,0,0,8,0,0,9,
            0,0,9,0,0,5,0,0,0,
            5,0,2,3,0,1,0,0,0,
            0,0,0,0,0,4,1,0,2
    };

    // http://www.websudoku.com/images/example-steps.html
    private static Integer[] hardPuzzle = {
            0,0,0,0,0,0,6,8,0,
            0,0,0,0,7,3,0,0,9,
            3,0,9,0,0,0,0,4,5,
            4,9,0,0,0,0,0,0,0,
            8,0,3,0,5,0,9,0,2,
            0,0,0,0,0,0,0,3,6,
            9,6,0,0,0,0,3,0,8,
            7,0,0,6,8,0,0,0,0,
            0,2,8,0,0,0,0,0,0
    };
    // reaches step 15 with disjoint subset testing
    /* Solution
            1,7,2,5,4,9,6,8,3
            6,4,5,8,7,3,2,1,9
            3,8,9,2,6,1,7,4,5
            4,9,6,3,2,7,8,5,1
            8,1,3,4,5,6,9,7,2
            2,5,7,1,9,8,4,3,6
            9,6,4,7,1,5,3,2,8
            7,3,1,6,8,2,5,9,4
            5,2,8,9,3,4,1,6,7
     */

    private static Integer[] customPuzzle = {
            0,0,0,0,0,2,0,0,8,
            0,3,5,0,0,0,0,7,0,
            0,0,0,9,0,0,0,0,0,
            0,0,0,0,0,0,5,0,0,
            0,6,0,0,8,0,0,0,3,
            0,0,0,7,0,4,0,0,0,
            8,0,0,0,0,0,0,0,0,
            0,0,1,0,0,0,0,4,0,
            3,0,0,0,1,0,0,0,0
    };
    /* Solutions
            4,9,7,5,3,2,1,6,8
            6,3,5,1,4,8,9,7,2
            1,8,2,9,7,6,4,3,5
            7,2,8,3,6,1,5,9,4
            9,6,4,2,8,5,7,1,3
            5,1,3,7,9,4,8,2,6
            8,4,9,6,2,7,3,5,1
            2,7,1,8,5,3,6,4,9
            3,5,6,4,1,9,2,8,7

            9,4,6,3,7,2,1,5,8
            2,3,5,8,4,1,9,7,6
            1,8,7,9,5,6,2,3,4
            4,1,8,6,2,3,5,9,7
            7,6,9,1,8,5,4,2,3
            5,2,3,7,9,4,6,8,1
            8,9,4,5,6,7,3,1,2
            6,7,1,2,3,9,8,4,5
            3,5,2,4,1,8,7,6,9
     */

    // http://www.conceptispuzzles.com/index.aspx?uri=info/article/452
    private static Integer[] hardestPuzzle = {
            0,0,0,0,0,0,0,1,5,
            0,2,0,0,6,0,0,0,0,
            0,0,0,0,0,0,4,0,8,
            0,0,3,0,0,0,9,0,0,
            0,0,0,1,0,0,0,0,0,
            0,0,0,0,0,8,0,0,0,
            1,5,0,4,0,0,0,0,0,
            0,0,0,0,7,0,3,0,0,
            8,0,0,0,0,0,0,6,0
    };
    /* Solution
            3,7,9,2,8,4,6,1,5
            4,2,8,5,6,1,7,3,9
            5,6,1,7,9,3,4,2,8
            2,1,3,6,5,7,9,8,4
            7,8,6,1,4,9,2,5,3
            9,4,5,3,2,8,1,7,6
            1,5,7,4,3,6,8,9,2
            6,9,2,8,7,5,3,4,1
            8,3,4,9,1,2,5,6,7
     */

    public static void main(String[] args) {
        Collection<Integer> puzzle;

        if (args.length == 1) {
            // Try to read puzzle from file
            try {
                puzzle = readPuzzle(args[0]);
            }
            catch (IOException ex) {
                System.out.println(ex.getMessage());
                return;
            }
        }
        else if (args.length == SudokuSolver.NUM_CELLS) {
            // Get puzzle from args
            puzzle = parsePuzzle(args);
        }
        else {
            System.out.println("Expected either the name of a file or the values of the puzzle as arguments (0s for unknown)");
            return;
        }

        try {
            System.out.println("Puzzle to be solved");
            printBoard(puzzle);

            System.out.println("\nBest Value First - With disjoint subset checking");
            SudokuSolver solver = new SudokuSolver(puzzle);
            solve(solver);

            System.out.println("Best Value First - No disjoint subset checking");
            solve(new NDSudokuSolver(puzzle));

            System.out.println("Best Variable First - With disjoint subset checking");
            solve(new BestVariableSolver(puzzle));

            System.out.println("Best Variable First - No disjoint subset checking");
            solve(new NDBestVariableSolver(puzzle));

//            System.out.println("First Variable - With disjoint subset checking");
//            solve(new FirstVariableSolver(puzzle));
//
//            System.out.println("First Variable - No disjoint subset checking");
//            solve(new NDFirstVariableSolver(puzzle));

            printBoard(solver);
        }
        catch (ConstraintViolationException ex) {
            System.out.println("Puzzle Initialized with constraint violation");
        }
    }

    private static void solve(SudokuSolver solver) {
        long startTime = System.nanoTime();
        boolean solved = solver.solve();
        double ms = (double)((System.nanoTime() - startTime) / 10000) / 100;

        if (solved) {
            System.out.println("Solved with " + solver.getRestores() + " backtracks and " + solver.getNumTested() + " values tested in " + ms + "ms\n");
        } else {
            System.out.println("Could not find a solution after " + solver.getRestores() + " backtracks and " + solver.getNumTested() + " values tested in " + ms + "ms\n");
        }
    }

    private static void printBoard(Collection<Integer> values) {
        int i = 1;
        for (Integer value : values) {
            String space;

            if (value == null || value == 0) {
                System.out.print("-");
            }
            else {
                System.out.print(value.intValue());
            }

            if (i % 9 == 0) {
                if (i / 9 == 9) {
                    space = "\n";
                }
                else if (i / 9 % 3 == 0) {
                    space = "\n----------|-----------|----------\n";
                }
                else {
                    space = "\n          |           |\n";
                }
            }
            else if (i % 3 == 0) {
                space = " | ";
            }
            else {
                space = "   ";
            }

            System.out.print(space);

            i++;
        }
    }

    private static void printBoard(ConstraintSolver solver) {
        int i = 1;
        for (Variable cell : solver.getVariables()) {
            Integer value = cell.getValue();
            String space;

            if (value == null || value == 0) {
                System.out.print("-");
            }
            else {
                System.out.print(value.intValue());
            }

            if (i % 9 == 0) {
                if (i / 9 == 9) {
                    space = "\n";
                }
                else if (i / 9 % 3 == 0) {
                    space = "\n----------|-----------|----------\n";
                }
                else {
                    space = "\n          |           |\n";
                }
            }
            else if (i % 3 == 0) {
                space = " | ";
            }
            else {
                space = "   ";
            }

            System.out.print(space);

            i++;
        }
    }

    private static void printSolution(Collection<Variable> cells) {
        int i = 1;
        for (Variable cell : cells) {
            Integer value = cell.getValue();
            String space;

            if (value == null) {
                System.out.print("?");
            }
            else {
                System.out.print(value.intValue());
            }

            if (i % 9 == 0) {
                space = "\n";
            }
            else {
                space = ",";
            }

            System.out.print(space);

            i++;
        }
    }

    private static Collection<Integer> parsePuzzle(String[] args) {
        Collection<Integer> result = new ArrayList<>();

        for (String arg : args) {
            result.add(Integer.parseInt(arg));
        }

        return result;
    }

    private static Collection<Integer> readPuzzle(String fileName) throws IOException {
        // Only takes the first NUM_CELLS values from the file
        Collection<Integer> result = new ArrayList<>();

        FileInputStream in = new FileInputStream(fileName);

        int n = 0;
        int c;

        try {
            while ((c = in.read()) != -1 && n < SudokuSolver.NUM_CELLS) {
                int val = c - '0';

                if (val >= 0 && val <= 9) {
                    result.add(val);
                    n++;
                }
            }
        } catch (IOException ex) {
            in.close();

            throw ex;
        }

        in.close();

        return result;
    }
}
