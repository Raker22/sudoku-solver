package com.floor8.sudoku;

import java.util.Collection;

public class NDBestVariableSolver extends BestVariableSolver {
    public NDBestVariableSolver(Collection<Integer> values) {
        super(values);
    }

    @Override
    protected void checkDisjointSubsets() {
        // Do nothing about disjoint subsets
    }
}
