package com.floor8.sudoku;

import java.util.Collection;

public class NDFirstVariableSolver extends FirstVariableSolver {
    public NDFirstVariableSolver(Collection<Integer> values) {
        super(values);
    }

    @Override
    protected void checkDisjointSubsets() {
        // No disjoint subset checking
    }
}
