package com.floor8.sudoku;

import com.floor8.constraint.*;

import java.util.*;

public class SudokuSolver extends AbstractSolver {
    public static final int SQUARE_SIZE = 3; // 3 x 3
    public static final int BOARD_SIZE = SQUARE_SIZE * SQUARE_SIZE; // SQUARE_SIZE x SQUARE_SIZE
    public static final int NUM_CELLS = BOARD_SIZE * BOARD_SIZE; // BOARD_SIZE^2

    private Stack<State> states = new Stack<>();
    private int numTested = 0;
    private int restores = 0;

    public SudokuSolver(Collection<Integer> values) {
        Variable[] cells = new Variable[NUM_CELLS];
        Collection<GlobalConstraint> rows = new ArrayList<>();
        Collection<GlobalConstraint> columns = new ArrayList<>();
        Collection<GlobalConstraint> squares = new ArrayList<>();

        if (values.size() == NUM_CELLS) {
            int cell = 0;
            for (Integer value : values) {
                cells[cell++] = new Cell(value == 0 ? null : value);
            }

            for (int i = 0; i < BOARD_SIZE; i++) {
                // Add AllDiffConstraints
                Collection<Variable> rowVars = new ArrayList<>();
                Collection<Variable> colVars = new ArrayList<>();
                Collection<Variable> squareVars = new ArrayList<>();

                int squareRow = i % SQUARE_SIZE;
                int squareCol = i / SQUARE_SIZE;
                int startIndex = squareRow * SQUARE_SIZE + squareCol * BOARD_SIZE * SQUARE_SIZE;

                for (int c = 0; c < BOARD_SIZE; c++) {
                    // Get row variables
                    rowVars.add(cells[i * BOARD_SIZE + c]);
                }

                for (int r = 0; r < BOARD_SIZE; r++) {
                    // Get column variables
                    colVars.add(cells[r * BOARD_SIZE + i]);
                }

                for (int s = 0; s < BOARD_SIZE; s++) {
                    // Get square variables
                    int subRow = s / SQUARE_SIZE;
                    int subCol = s % SQUARE_SIZE;
                    int index = startIndex + subRow * BOARD_SIZE + subCol;

                    squareVars.add(cells[index]);
                }

                GlobalConstraint row = new AllDiffConstraint(rowVars);
                GlobalConstraint column = new AllDiffConstraint(colVars);
                GlobalConstraint square = new AllDiffConstraint(squareVars);

                row.generateBinaryConstraints(); // Make not equal constraints for each row
                column.generateBinaryConstraints(); // Make not equal constraints for each column

                rows.add(row);
                columns.add(column);
                squares.add(square);
            }

            for (int i = 0; i < NUM_CELLS; i++) {
                // We need to manually add the NotEqualConstraints for squares so we don't have duplicates
                Variable dependent = cells[i];
                int row = i / BOARD_SIZE;
                int col = i % BOARD_SIZE;
                int squareRow = row / SQUARE_SIZE;
                int squareCol = col / SQUARE_SIZE;
                int subRow = row - squareRow * SQUARE_SIZE;
                int subCol = col - squareCol * SQUARE_SIZE;

                for (int s = 0; s < BOARD_SIZE; s++) {
                    // Add constraints for the square
                    int r = s / SQUARE_SIZE;
                    int c = s % SQUARE_SIZE;

                    if (r != subRow && c != subCol) {
                        // Don't add cells in the same row/col since they've already been added
                        int independentRow = SQUARE_SIZE * squareRow + r;
                        int independentCol = SQUARE_SIZE * squareCol + c;
                        Variable independent = cells[independentRow * BOARD_SIZE + independentCol];

                        new NotEqualConstraint(independent, dependent);
                    }
                }
            }

            this.setVariables(Arrays.asList(cells));

            Collection<GlobalConstraint> globalConstraints = this.getGlobalConstraints();
            globalConstraints.addAll(rows);
            globalConstraints.addAll(columns);
            globalConstraints.addAll(squares);
        }
        else {
            throw new IllegalArgumentException("Size of values passed to SudokuSolver must be " + NUM_CELLS + " but got " + values.size());
        }
    }

    @Override
    public boolean solve() {
        this.checkDisjointSubsets();

        this.saveState();

        while (this.getUnsolvedVariables().size() > 0 && this.states.size() > 0) {
            // Run until we've solved the problem or have no more states to revert to
            Collection<Integer> testValues = this.states.peek().getTestValues();

            if (testValues.size() == 0) {
                // Check all but the last state to avoid EmptyStackException on peek
                while (this.states.size() > 1 && testValues.size() == 0) {
                    // There may be more states with no test values
                    this.states.pop();
                    testValues = this.states.peek().getTestValues();
                }

                if (testValues.size() == 0) {
                    // Check the last state on the stack
                    this.states.pop();
                }

                if (this.states.size() > 0) {
                    // Restore to the point we popped to
                    this.restoreState();
                }
            }

            if (this.states.size() > 0) {
                State state = this.states.peek();
                Variable testVariable = state.getTestVariable();
                Integer testValue = testValues.iterator().next();

                testValues.remove(testValue);

                this.numTested++;

                try {
                    // Try updating the value of the test variable
                    testVariable.update(testValue);
                    this.checkDisjointSubsets();

                    // Save the state if we succeed
                    this.saveState();
                }
                catch (ConstraintViolationException ex) {
                    // Restore if there is a problem updating
                    this.restoreState();
                }
            }
        }

        return this.solved();
    }

    protected void checkDisjointSubsets() {
        int maxSize = 8;
        boolean updated = true;

        while (updated) {
            updated = false;

            // Single hidden subsets
//            for (Iterator<GlobalConstraint> it = this.getGlobalConstraints().iterator(); it.hasNext() && !updated; ) {
//                AllDiffConstraint constraint = (AllDiffConstraint) it.next();
//                updated = constraint.checkHiddenSubsets(1);
//            }

            for (int setSize = 2; setSize < maxSize && !updated; setSize++) {
                // Start with smallest subsets first
                // Visible subsets
                for (Iterator<GlobalConstraint> it = this.getGlobalConstraints().iterator(); it.hasNext() && !updated; ) {
                    AllDiffConstraint constraint = (AllDiffConstraint) it.next();
                    updated = constraint.checkVisibleSubsets(setSize);
                }

                // Hidden subsets
//                for (Iterator<GlobalConstraint> it = this.getGlobalConstraints().iterator(); it.hasNext() && !updated; ) {
//                    AllDiffConstraint constraint = (AllDiffConstraint) it.next();
//                    updated = constraint.checkHiddenSubsets(setSize);
//                }
            }
        }
    }

    protected boolean solved() {
        return this.getUnsolvedVariables().size() == 0;
    }

    protected void saveState() {
        // Save the current state of the problem onto the stack
        Collection<Integer> testValues = new ArrayList<>();
        Variable testVariable = this.getTestVariableValues(testValues);
        Collection<Integer> values = new ArrayList<>();
        Collection<DiscreteDomain> domains = new ArrayList<>();

        for (Variable variable : this.getVariables()) {
            // Copy value and domain
            DiscreteDomain domain = new GenericDiscreteDomain();
            domain.getValues().addAll(variable.getDomain().getValues());

            values.add(variable.getValue());
            domains.add(domain);
        }

        this.states.push(new ProblemState(testVariable, testValues, values, domains));
    }

    protected void restoreState() {
        // Restore to the most recent state
        State state = states.peek();
        Iterator<Integer> valueIt = state.getValues().iterator();
        Iterator<DiscreteDomain> domainIt = state.getDomains().iterator();

        for (Variable variable : this.getVariables()) {
            variable.setValue(valueIt.next());
            variable.setDomain(domainIt.next());
        }

        this.restores++;
    }

    public int getNumTested() {
        return this.numTested;
    }

    public int getRestores() {
        return this.restores;
    }
}
