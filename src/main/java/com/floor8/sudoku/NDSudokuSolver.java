package com.floor8.sudoku;

import java.util.Collection;

public class NDSudokuSolver extends SudokuSolver {
    public NDSudokuSolver(Collection<Integer> values) {
        super(values);
    }

    @Override
    protected void checkDisjointSubsets() {
        // Don't check subsets and see how we do
    }
}
