package com.floor8.sudoku;

import com.floor8.constraint.Variable;

import java.util.Collection;

public class FirstVariableSolver extends SudokuSolver {
    public FirstVariableSolver(Collection<Integer> values) {
        super(values);
    }

    @Override
    public Variable getTestVariableValues(Collection<Integer> values) {
        Collection<Variable> unsolved = this.getUnsolvedVariables();
        Variable result = null;

        if (unsolved.size() > 0) {
            result = unsolved.iterator().next();
            values.addAll(result.getDomain().getValues());
        }

        return result;
    }
}
