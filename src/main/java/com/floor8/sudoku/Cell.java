package com.floor8.sudoku;

import com.floor8.constraint.AbstractVariable;
import com.floor8.constraint.GenericDiscreteDomain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Cell extends AbstractVariable {
    public static final Collection<Integer> VALUES = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

    public Cell() {
        this.setDomain(new GenericDiscreteDomain(new ArrayList<>(VALUES)));
    }

    public Cell(Integer value) {
        this();
        this.update(value);
    }
}
