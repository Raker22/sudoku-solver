package com.floor8.sudoku;

import com.floor8.constraint.Variable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class BestVariableSolver extends SudokuSolver {
    public BestVariableSolver(Collection<Integer> values) {
        super(values);
    }

    @Override
    public Variable getTestVariableValues(Collection<Integer> values) {
        Collection<Variable> unsolved = this.getUnsolvedVariables();
        Variable bestVar = null;

        if (unsolved.size() > 0) {
            // Only run if we have variables to work with
            List<Variable> minVariables = new ArrayList<>();
            Iterator<Variable> unsolvedIt = unsolved.iterator();
            int min = unsolvedIt.next().getDomain().getValues().size();

            while (unsolvedIt.hasNext()) {
                // Find the variables with the minimum number of domain values
                Variable variable = unsolvedIt.next();
                int size = variable.getDomain().getValues().size();

                if (size < min) {
                    minVariables.clear();
                    minVariables.add(variable);
                    min = size;
                } else if (size == min) {
                    minVariables.add(variable);
                }
            }

            bestVar = minVariables.get(0);
            values.addAll(bestVar.getDomain().getValues());
        }

        return bestVar;
    }
}
