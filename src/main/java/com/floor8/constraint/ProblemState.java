package com.floor8.constraint;

import java.util.Collection;

public class ProblemState extends AbstractState {
    public ProblemState(Variable testVariable, Collection<Integer> testValues, Collection<Integer> values, Collection<DiscreteDomain> domains) {
        this.setTestVariable(testVariable);
        this.setTestValues(testValues);
        this.setValues(values);
        this.setDomains(domains);
    }
}
