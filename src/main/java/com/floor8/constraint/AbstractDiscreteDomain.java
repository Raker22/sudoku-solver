package com.floor8.constraint;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractDiscreteDomain implements DiscreteDomain {
    private Collection<Integer> values = new ArrayList<>();

    @Override
    public Collection<Integer> getValues() {
        return this.values;
    }

    @Override
    public void setValues(Collection<Integer> values) {
        this.values = values;
    }

    @Override
    public boolean removeValue(Integer value) {
        return this.values.remove(value);
    }
}
