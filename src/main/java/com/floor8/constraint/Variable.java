package com.floor8.constraint;

import java.util.Collection;

public interface Variable {
    /**
     * Returns the current value of the object
     * @return the value of the object
     */
    Integer getValue();

    /**
     * Sets the value of the object without affecting dependents
     * @param value the new value of the object
     */
    void setValue(Integer value);

    /**
     * Sets the value of the object and updates dependents
     * @param value the new value of the object
     * @return false if the problem becomes unsolvable due to the update, true otherwise
     */
    void update(Integer value);

    /**
     * Returns the constraints where this object is the independent part
     * of the constraint
     * ({@code this == constraint.getIndependent()}
     * @return the constraints where this is the independent
     */
    Collection<BinaryConstraint> getIndependentConstraints();
    boolean addIndependentConstraint(BinaryConstraint constraint);

    /**
     * Get the constraints where this object is the dependent part
     * of the constraint
     * ({@code this == constraint.getDependent()}
     * @return the constraints where this is the dependent
     */
    Collection<BinaryConstraint> getDependentConstraints();
    boolean addDependentConstraint(BinaryConstraint constraint);

    /**
     * Returns the global constraints that this object is part of
     * @return global constraints involving this object
     */
    Collection<GlobalConstraint> getGlobalConstraints();
    boolean addGlobalConstraint(GlobalConstraint constraint);

    /**
     * Returns all binary constraints this object is associated with independent and dependent
     * @return the independent and dependent binary constraints on this object
     */
    Collection<BinaryConstraint> getConstraints();

    /**
     * Returns the domain of the object
     * @return the domain of the object
     */
    DiscreteDomain getDomain();
    void setDomain(DiscreteDomain domain);

    /**
     * Remove the value from the domain and do any necessary updating
     * @param value the value to remove
     */
    boolean removeDomainValue(Integer value);

    /**
     * Gets the number of conflicts for a specific value
     * @param value the value to look for in dependent Constrainables' domains
     * @return the number of variables it affects
     */
    int getNumConflictsFor(Integer value);

    /**
     * Get the domain values in the order they should be tested in
     * @return the domain values in the order they should be tested in
     */
    Collection<Integer> getOrderedDomainValues();
}
