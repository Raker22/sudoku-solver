package com.floor8.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractVariable implements Variable {
    private Integer value = null;
    private DiscreteDomain domain = new GenericDiscreteDomain();
    private Collection<BinaryConstraint> independentConstraints = new ArrayList<>();
    private Collection<BinaryConstraint> dependentConstraints = new ArrayList<>();
    private Collection<GlobalConstraint> globalConstraints = new ArrayList<>();

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public void update(Integer value) {
        this.setValue(value);

        if (value != null) {
            // Only update if we're actually setting the value
            this.getDomain().getValues().clear();

            for (BinaryConstraint constraint : this.getIndependentConstraints()) {
                // Update each constraint
                constraint.update();
            }
            for (GlobalConstraint constraint : this.getGlobalConstraints()) {
                // Update global constraints
                constraint.update(this);
            }
        }
    }

    @Override
    public Collection<BinaryConstraint> getIndependentConstraints() {
        return this.independentConstraints;
    }

    @Override
    public boolean addIndependentConstraint(BinaryConstraint constraint) {
        return this.independentConstraints.add(constraint);
    }

    @Override
    public Collection<BinaryConstraint> getDependentConstraints() {
        return this.dependentConstraints;
    }

    @Override
    public boolean addDependentConstraint(BinaryConstraint constraint) {
        return this.dependentConstraints.add(constraint);
    }

    @Override
    public Collection<GlobalConstraint> getGlobalConstraints() {
        return this.globalConstraints;
    }

    @Override
    public boolean addGlobalConstraint(GlobalConstraint constraint) {
        return this.globalConstraints.add(constraint);
    }

    @Override
    public Collection<BinaryConstraint> getConstraints() {
        // Get constraints where this is the independent part of the constraint
        Collection<BinaryConstraint> constraints = new ArrayList<>(this.independentConstraints);

        // Get constraints where this is the dependent part of the constraint
        constraints.addAll(this.getDependentConstraints());

        return constraints;
    }

    @Override
    public DiscreteDomain getDomain() {
        return this.domain;
    }

    @Override
    public void setDomain(DiscreteDomain domain) {
        this.domain = domain;
    }

    /**
     * Remove the value from the domain. If the domain only has one value
     * remaining set the constrainable to that value.
     * @param value the value to remove
     */
    @Override
    public boolean removeDomainValue(Integer value) {
        boolean result = this.getDomain().removeValue(value);

        if (this.getDomain().getValues().size() == 1) {
            this.update(this.getDomain().getValues().iterator().next());
        }

        return result;
    }

    @Override
    public int getNumConflictsFor(Integer value) {
        int result = 0;

        for (BinaryConstraint constraint : this.getIndependentConstraints()) {
            if (constraint.getDependent().getDomain().getValues().contains(value)) {
                result++;
            }
        }

        return result;
    }

    @Override
    public Collection<Integer> getOrderedDomainValues() {
        List<Integer> domainValues = new ArrayList<>();
        List<Integer> conflicts = new ArrayList<>();
        Collection<Integer> result = new ArrayList<>();

        for (Integer domainValue : this.getDomain().getValues()) {
            // Get the number of conflicts for each domain value
            domainValues.add(domainValue);
            conflicts.add(this.getNumConflictsFor(domainValue));
        }

        while (domainValues.size() > 0) {
            // Until we run out of domain values keep getting the smallest one
            int minIndex = 0;
            Integer minConflicts = conflicts.get(0);

            for (int i = 1; i < conflicts.size(); i++) {
                Integer numConflicts = conflicts.get(i);

                if (numConflicts < minConflicts) {
                    minConflicts = numConflicts;
                    minIndex = i;
                }
            }

            result.add(domainValues.get(minIndex));
            conflicts.remove(minIndex);
            domainValues.remove(minIndex);
        }

        return result;
    }
}
