package com.floor8.constraint;

public class NotEqualConstraint extends AbstractBinaryConstraint {
    public NotEqualConstraint(Variable independent, Variable dependent) {
        super(independent, dependent);
        this.update();
    }

    @Override
    public void update() {
        Integer value = this.getIndependent().getValue();

        if (value != null) {
            if (!this.getDependent().removeDomainValue(value) && value == this.getDependent().getValue()) {
                // Throw an exception if the values conflict
                throw new ConstraintViolationException("NotEqualConstraint Violated");
            }
        }
    }
}
