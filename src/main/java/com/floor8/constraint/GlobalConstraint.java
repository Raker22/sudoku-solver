package com.floor8.constraint;

import java.util.Collection;

public interface GlobalConstraint {
    Collection<Variable> getVariables();
    void setVariables(Collection<Variable> variables);
    boolean update(Variable updated);
    Collection<BinaryConstraint> generateBinaryConstraints();
}
