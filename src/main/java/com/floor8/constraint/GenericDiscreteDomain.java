package com.floor8.constraint;

import java.util.Collection;

public class GenericDiscreteDomain extends AbstractDiscreteDomain {
    public GenericDiscreteDomain() {

    }

    public GenericDiscreteDomain(Collection<Integer> values) {
        this.setValues(values);
    }
}
