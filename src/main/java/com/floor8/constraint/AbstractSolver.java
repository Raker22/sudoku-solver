package com.floor8.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public abstract class AbstractSolver extends AbstractConstraintSolver {
    @Override
    public Variable getTestVariableValues(Collection<Integer> values) {
        Collection<Variable> unsolved = this.getUnsolvedVariables();
        Variable bestVar = null;

        if (unsolved.size() > 0) {
            // Only run if we have variables to work with
            Collection<Variable> minVariables = new ArrayList<>();
            Iterator<Variable> unsolvedIt = unsolved.iterator();
            int min = unsolvedIt.next().getDomain().getValues().size();

            while (unsolvedIt.hasNext()) {
                // Find the variables with the minimum number of domain values
                Variable variable = unsolvedIt.next();
                int size = variable.getDomain().getValues().size();

                if (size < min) {
                    minVariables.clear();
                    minVariables.add(variable);
                    min = size;
                }
                else if (size == min) {
                    minVariables.add(variable);
                }
            }

            Iterator<Variable> varIt = minVariables.iterator();
            bestVar = varIt.next();
            Iterator<Integer> domainIt = bestVar.getDomain().getValues().iterator();
            int minConflicts = bestVar.getNumConflictsFor(domainIt.next());

            while (domainIt.hasNext()) {
                Integer domainValue = domainIt.next();
                int numConflicts = bestVar.getNumConflictsFor(domainValue);

                if (numConflicts < minConflicts) {
                    minConflicts = numConflicts;
                }
            }

            while (varIt.hasNext()) {
                Variable variable = varIt.next();

                for (Integer domainValue : variable.getDomain().getValues()) {
                    int numConflicts = variable.getNumConflictsFor(domainValue);

                    if (numConflicts < minConflicts) {
                        minConflicts = numConflicts;
                        bestVar = variable;
                    }
                }
            }

            values.addAll(bestVar.getOrderedDomainValues());
        }

        return bestVar;
    }
}
