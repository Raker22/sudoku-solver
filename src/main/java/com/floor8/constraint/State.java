package com.floor8.constraint;

import java.util.Collection;

public interface State {
    Variable getTestVariable();
    void setTestVariable(Variable variable);
    Collection<Integer> getTestValues();
    void setTestValues(Collection<Integer> values);
    Collection<Integer> getValues();
    void setValues(Collection<Integer> values);
    Collection<DiscreteDomain> getDomains();
    void setDomains(Collection<DiscreteDomain> domains);
}
