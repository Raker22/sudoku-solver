package com.floor8.constraint;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractGlobalConstraint implements GlobalConstraint {
    private Collection<Variable> variables = new ArrayList<>();

    @Override
    public Collection<Variable> getVariables() {
        return this.variables;
    }

    @Override
    public void setVariables(Collection<Variable> variables) {
        this.variables = variables;
    }
}
