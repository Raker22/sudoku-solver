package com.floor8.constraint;

public interface BinaryConstraint {
    Variable getDependent(); // The dependent variable
    Variable getIndependent(); // The independent variable
    void update();
}
