package com.floor8.constraint;

public abstract class AbstractBinaryConstraint implements BinaryConstraint {
    private final Variable independent;
    private final Variable dependent;

    public AbstractBinaryConstraint(Variable independent, Variable dependent) {
        this.independent = independent;
        this.dependent = dependent;

        this.independent.addIndependentConstraint(this);
        this.dependent.addDependentConstraint(this);
    }

    @Override
    public Variable getDependent() {
        return this.dependent;
    }

    @Override
    public Variable getIndependent() {
        return this.independent;
    }
}
