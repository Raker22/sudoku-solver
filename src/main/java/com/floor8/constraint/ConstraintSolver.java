package com.floor8.constraint;

import com.sun.istack.internal.NotNull;

import java.util.Collection;

public interface ConstraintSolver {
    // Getters/Setters
    Collection<Variable> getVariables();
    void setVariables(Collection<Variable> variables);
    Collection<BinaryConstraint> getBinaryConstraints();
    void setBinaryConstraints(Collection<BinaryConstraint> constraints);
    Collection<GlobalConstraint> getGlobalConstraints();
    void setGlobalConstraints(Collection<GlobalConstraint> constraints);

    Collection<Variable> getUnsolvedVariables();

    /**
     * Solves the constraint satisfaction problem (variables will be set to a value that meets all constraints)
     * @return true if the problem was solvable, false otherwise
     */
    boolean solve();

    /**
     * Gets the variable and values to test in the order they should be tested in
     * @param values the values to be tested in the order they should be tested in
     * @return the variable to be tested
     */
    Variable getTestVariableValues(@NotNull Collection<Integer> values);
}
