package com.floor8.constraint;

import java.util.*;

public class AllDiffConstraint extends AbstractGlobalConstraint {
    public AllDiffConstraint() {

    }

    public AllDiffConstraint(Collection<Variable> variables) {
        this.setVariables(variables);
    }

    @Override
    public boolean update(Variable updated) {
        return true;
    }

    @Override
    public Collection<BinaryConstraint> generateBinaryConstraints() {
        Collection<BinaryConstraint> constraints = new ArrayList<>();

        for (Variable independent : this.getVariables()) {
            for (Variable dependent : this.getVariables()) {
                if (independent != dependent) {
                    constraints.add(new NotEqualConstraint(independent, dependent));
                }
            }
        }

        return constraints;
    }

    public Collection<Variable> remainingVariables() {
        Collection<Variable> variables = new ArrayList<>();

        for (Variable variable : this.getVariables()) {
            if (variable.getValue() == null) {
                variables.add(variable);
            }
        }

        return variables;
    }

    /**
     * Looks for the first visible disjoint subset of a given size. The variables associated with the disjoint subset
     * are added to variables and the domain values of the variables are added to domainValues. Returns true if the
     * domain of any variable is updated, false otherwise.
     * @param setSize the size of the subset to look for
     * @param variables the collection to add the variables that are part of the subset into
     * @param domainValues the collection to add the domain values that the set consists of
     * @return true if any variable's domain is updated, false otherwise
     */
    public boolean checkVisibleSubsets(int setSize, Collection<Variable> variables, Collection<Integer> domainValues) {
        List<Variable> validVariables = new ArrayList<>();
        boolean updated = false;

        // Look for disjoint subsets of size setSize
        for (Variable variable : this.getVariables()) {
            if (variable.getValue() == null && variable.getDomain().getValues().size() <= setSize) {
                // Only check variables that have the correct number of domain values
                validVariables.add(variable);
            }
        }

        for (int i = 0; i < validVariables.size() && !updated; i++) {
            Variable variable = validVariables.get(i);
            List<Variable> variableSet = new ArrayList<>();
            Collection<Integer> domainSet = new ArrayList<>();

            // Start with only the initial variable and domain values
            variableSet.add(variable);
            domainSet.addAll(variable.getDomain().getValues());

            if (visibleDisjointSubset(i + 1, variableSet, domainSet, setSize, validVariables)) {
                for (Variable variableToUpdate : this.getVariables()) {
                    // Remove the domainValues from the other variables
                    if (!variableSet.contains(variableToUpdate)) {
                        for (Integer value : domainSet) {
                            // If we don't remove any domain values the problem hasn't been updated
                            updated |= variableToUpdate.removeDomainValue(value);
                        }
                    }
                }

                if (updated) {
                    if (variables != null) {
                        // Copy the variables if possible
                        variables.addAll(variableSet);
                    }

                    if (domainValues != null) {
                        // Copy the domainValues if possible
                        domainValues.addAll(domainSet);
                    }
                }
            }
        }

        return updated;
    }

    /**
     * Looks for the first hidden disjoint subset of a given size. The variables associated with the disjoint subset
     * are added to variables and the domain values of the variables are added to domainValues. Returns true if the
     * domain of any variable is updated, false otherwise.
     * @param setSize the size of the subset to look for
     * @param variables the collection to add the variables that are part of the subset into
     * @param domainValues the collection to add the domain values that the set consists of
     * @return true if any variable's domain is updated, false otherwise
     */
    public boolean checkHiddenSubsets(int setSize, Collection<Variable> variables, Collection<Integer> domainValues) {
        List<Variable> validVariables = new ArrayList<>();
        boolean updated = false;

        // Look for disjoint subsets of size setSize
        for (Variable variable : this.getVariables()) {
            if (variable.getValue() == null && variable.getDomain().getValues().size() >= setSize) {
                // Only check variables that have the correct number of domain values
                validVariables.add(variable);
            }
        }

        for (int i = 0; i < validVariables.size() && !updated; i++) {
            Variable variable = validVariables.get(i);
            List<Variable> variableSet = new ArrayList<>();
            Collection<Integer> domainSet = new ArrayList<>();

            // Start with only the initial variable and domain values
            variableSet.add(variable);
            domainSet.addAll(variable.getDomain().getValues());

            if (i == 2) {
                System.out.print("");
            }

            if (this.hiddenDisjointSubset(i + 1, variableSet, domainSet, setSize, validVariables)) {
                for (Variable variableToUpdate : variableSet) {
                    // Set the variable's domains to domainValues
                    if (!variableToUpdate.getDomain().getValues().equals(domainSet)) {
                        updated = true;
                        variableToUpdate.setDomain(new GenericDiscreteDomain(domainSet));
                    }
                }

                if (updated) {
                    if (variables != null) {
                        // Copy the variables if possible
                        variables.addAll(variableSet);
                    }

                    if (domainValues != null) {
                        // Copy the domainValues if possible
                        domainValues.addAll(domainSet);
                    }
                }
            }
        }

        return updated;
    }

    public boolean checkVisibleSubsets(Collection<Variable> variables, Collection<Integer> domainValues) {
        int maxSize = this.remainingVariables().size() - 1;
        boolean updated = false;

        for (int setSize = 2; setSize <= maxSize && !updated; setSize++) {
            // Ignore single subsets as they can be handled by NotEqualConstraints
            updated = this.checkVisibleSubsets(setSize, variables, domainValues);
        }

        return updated;
    }

    public boolean checkVisibleSubsets(int setSize) {
        return this.checkVisibleSubsets(setSize, null, null);
    }

    public boolean checkVisibleSubsets() {
        return this.checkVisibleSubsets(null, null);
    }

    public boolean checkVisibleSubsetsUpTo(int maxSize, Collection<Variable> variables, Collection<Integer> domainValues) {
        boolean updated = false;
        int hardMax = this.remainingVariables().size() - 1;

        if (maxSize > hardMax) {
            maxSize = hardMax;
        }

        for (int setSize = 2; setSize <= maxSize && !updated; setSize++) {
            // Check all subsets of size [2, maxSize]
            updated = this.checkVisibleSubsets(setSize, variables, domainValues);
        }

        return updated;
    }

    public Collection<Variable> checkVisibleSubsetsUpTo(int maxSize) {
        Collection<Variable> variables = new ArrayList<>();

        this.checkVisibleSubsetsUpTo(maxSize, variables, null);

        return variables;
    }

    public boolean checkHiddenSubsets(Collection<Variable> variables, Collection<Integer> domainValues) {
        int maxSize = this.remainingVariables().size() - 1;
        boolean updated = false;

        for (int setSize = 2; setSize <= maxSize && !updated; setSize++) {
            // Ignore single subsets as they can be handled by NotEqualConstraints
            updated = this.checkHiddenSubsets(setSize, variables, domainValues);
        }

        return updated;
    }

    public boolean checkHiddenSubsets(int setSize) {
        return this.checkHiddenSubsets(setSize, null, null);
    }

    public boolean checkHiddenSubsets() {
        return this.checkHiddenSubsets(null, null);
    }

    public boolean checkHiddenSubsetsUpTo(int maxSize, Collection<Variable> variables, Collection<Integer> domainValues) {
        boolean updated = false;
        int hardMax = this.remainingVariables().size() - 1;

        if (maxSize > hardMax) {
            maxSize = hardMax;
        }

        for (int setSize = 2; setSize <= maxSize && !updated; setSize++) {
            // Check all subsets of size [2, maxSize]
            updated = this.checkHiddenSubsets(setSize, variables, domainValues);
        }

        return updated;
    }

    public Collection<Variable> checkHiddenSubsetsUpTo(int maxSize) {
        Collection<Variable> variables = new ArrayList<>();

        this.checkHiddenSubsetsUpTo(maxSize, variables, null);

        return variables;
    }

    public boolean hiddenDisjointSubset(int startIndex, List<Variable> currentSet, Collection<Integer> domainValues, int setSize, List<Variable> variables) {
        // Recursively check sets of variables to see if there is a "hidden" disjoint subset
        int numInDomain = domainValues.size();
        int depth = currentSet.size() + 1; // 1 for first variable being considered, 2 for second, etc.
        boolean valid = false; // Valid candidate for a subset

        for (int i = startIndex; i < variables.size() && !valid; i++) {
            // From startIndex run through all of the remaining variables
            Variable variable = variables.get(i);
            Collection<Integer> extraValues = new ArrayList<>();

            valid = true;

            for (Iterator<Integer> it = domainValues.iterator(); it.hasNext() && valid;) {
                Integer value = it.next();

                if (!variable.getDomain().getValues().contains(value)) {
                    numInDomain--;

                    if (numInDomain < setSize) {
                        valid = false;
                    }
                    else {
                        extraValues.add(value);
                    }
                }
            }


            if (valid) {
                // If this variable passed try adding it to the set
                currentSet.add(variable);
                domainValues.removeAll(extraValues);

                if (depth < setSize) {
                    // We still need to look at more variables
                    if (!hiddenDisjointSubset(i + 1, currentSet, domainValues, setSize, variables)) {
                        // If we fail going deeper then try the next value
                        currentSet.remove(currentSet.size() - 1);
                        domainValues.addAll(extraValues);

                        valid = false;
                    }
                    // Otherwise we found some passing set of variables
                }
                else {
                    // Found the variables we need now check that the domainValues aren't in other variables' domains
                    for (Variable otherVariable : this.getVariables()) {
                        if (!currentSet.contains(otherVariable)) {
                            // Remove domain values that other variables have in their domain
                            for (Iterator<Integer> it = otherVariable.getDomain().getValues().iterator(); it.hasNext() && valid;) {
                                Integer value = it.next();

                                if (domainValues.remove(value)) {
                                    // Save the value if it was removed
                                    extraValues.add(value);

                                    if (domainValues.size() < setSize) {
                                        // If the other variables reduced the domain too much move to the next value
                                        currentSet.remove(currentSet.size() - 1);
                                        domainValues.addAll(extraValues);

                                        valid = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return valid;
    }

    public static boolean visibleDisjointSubset(int startIndex, List<Variable> currentSet, Collection<Integer> domainValues, int setSize, List<Variable> variables) {
        // Recursively check sets of variables to see if there is a "naked" disjoint subset
        int numInDomain = domainValues.size();
        int depth = currentSet.size() + 1; // 1 for first variable being considered, 2 for second, etc.
        boolean valid = false; // Valid candidate for a subset

        for (int i = startIndex; i < variables.size() && !valid; i++) {
            // From startIndex run through all of the remaining variables
            Variable variable = variables.get(i);
            Collection<Integer> extraValues = new ArrayList<>();

            valid = true;

            for (Iterator<Integer> it = variable.getDomain().getValues().iterator(); it.hasNext() && valid;) {
                // For each domain value of the variable
                Integer value = it.next();

                if (!domainValues.contains(value)) {
                    // If the value isn't in the current domainValues it will nedd to be added
                    numInDomain++;

                    if (numInDomain > setSize) {
                        // If there are too many values stop
                        valid = false;
                    }
                    else {
                        // If the value can be added set it aside for later
                        extraValues.add(value);
                    }
                }
            }

            if (valid) {
                // If this variable passed try adding it to the set
                currentSet.add(variable);
                domainValues.addAll(extraValues);

                if (depth < setSize) {
                    // We still need to look at more variables
                    if (!visibleDisjointSubset(i + 1, currentSet, domainValues, setSize, variables)) {
                        // If we fail going deeper then try the next value
                        currentSet.remove(currentSet.size() - 1);
                        domainValues.removeAll(extraValues);

                        valid = false;
                    }
                    // Otherwise we found some passing set of variables
                }
                // Otherwise we've found all the variables we need
            }
        }

        return valid;
    }
}
