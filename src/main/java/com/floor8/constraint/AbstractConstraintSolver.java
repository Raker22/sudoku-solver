package com.floor8.constraint;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractConstraintSolver implements ConstraintSolver {
    protected Collection<Variable> variables = new ArrayList<>();
    protected Collection<BinaryConstraint> binaryConstraints = new ArrayList<>();
    protected Collection<GlobalConstraint> globalConstraints = new ArrayList<>();

    @Override
    public Collection<Variable> getVariables() {
        return this.variables;
    }

    @Override
    public void setVariables(Collection<Variable> variables) {
        this.variables = variables;
    }

    @Override
    public Collection<BinaryConstraint> getBinaryConstraints() {
        return this.binaryConstraints;
    }

    @Override
    public void setBinaryConstraints(Collection<BinaryConstraint> constraints) {
        this.binaryConstraints = constraints;
    }

    @Override
    public Collection<GlobalConstraint> getGlobalConstraints() {
        return this.globalConstraints;
    }

    @Override
    public void setGlobalConstraints(Collection<GlobalConstraint> constraints) {
        this.globalConstraints = constraints;
    }

    @Override
    public Collection<Variable> getUnsolvedVariables() {
        Collection<Variable> result = new ArrayList<>();

        for (Variable variable : this.getVariables()) {
            if (variable.getValue() == null) {
                result.add(variable);
            }
        }

        return result;
    }
}
