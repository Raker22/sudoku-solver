package com.floor8.constraint;

import java.util.Collection;

public abstract class AbstractState implements State {
    private Variable testVariable;
    private Collection<Integer> testValues;
    private Collection<Integer> values;
    private Collection<DiscreteDomain> domains;

    @Override
    public Variable getTestVariable() {
        return this.testVariable;
    }

    @Override
    public void setTestVariable(Variable variable) {
        this.testVariable = variable;
    }

    @Override
    public Collection<Integer> getTestValues() {
        return this.testValues;
    }

    @Override
    public void setTestValues(Collection<Integer> values) {
        this.testValues = values;
    }

    @Override
    public Collection<Integer> getValues() {
        return this.values;
    }

    @Override
    public void setValues(Collection<Integer> values) {
        this.values = values;
    }

    @Override
    public Collection<DiscreteDomain> getDomains() {
        return this.domains;
    }

    @Override
    public void setDomains(Collection<DiscreteDomain> domains) {
        this.domains = domains;
    }
}
