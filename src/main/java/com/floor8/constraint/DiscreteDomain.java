package com.floor8.constraint;

import java.util.Collection;

public interface DiscreteDomain {
    Collection<Integer> getValues();
    void setValues(Collection<Integer> values);
    boolean removeValue(Integer value);
}
